//
//  AddViewController.swift
//  Life Items
//
//  Created by Casey Scott on 3/4/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//

import UIKit
import CoreData

protocol UpdateListener {
    func updateTable()
}

internal let noDescription = "No description"

class AddViewController: UIViewController {
    
    
    let context = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
    var catagories: [String] = []
    var selectedCategory = ""
    var delegate: UpdateListener?
    var currentString = ""
    
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var categoriesSpinner: UIPickerView!
    @IBOutlet weak var costEdit: UITextField!
    @IBOutlet weak var titleRequiredLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        categoriesSpinner.dataSource = self
        categoriesSpinner.delegate = self
        selectedCategory = catagories[0]
    }
    
    @IBAction func closeKeyboardTapped(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func addItemButtonTap(_ sender: Any) {
        if let name = titleField.text{
            if name == ""{
                titleField.shake()
                return
            }
        }
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "RareItem", in: managedContext)
        let itemMain = NSManagedObject(entity: entity!, insertInto: managedContext) as? RareItem
        
        if let item = itemMain{
            if let name = titleField.text{
                item.itemName = name
            }
            if let desc = descriptionField.text{
                if desc == ""{
                    item.itemDesc = noDescription
                }else{
                    item.itemDesc = desc
                }
            }
            let date = NSDate(timeIntervalSinceNow: 0)
            item.createdDate = date
            
            
            item.cost = NSDecimalNumber(string: costEdit.text!)
            print(String(describing: item.cost))

            item.category = Category(context: managedContext)
            item.category?.categoryName = selectedCategory
            print(item.category?.categoryName as Any)
            
            item.notes = " "
            
            do{
                try managedContext.save()
            }catch{
                NSLog("Unresolved error \(String(describing: error)), \(error.localizedDescription)")
            }
        }
        resetFeilds()
        delegate?.updateTable()
        self.performSegue(withIdentifier: "unwindToMain", sender: self)
    }
    
    func resetFeilds(){
        titleField.text = ""
        descriptionField.text = ""
        costEdit.text = "0.00"
    }
}

extension AddViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return catagories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return catagories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCategory = catagories[row]
        print("Selected catagory: \(selectedCategory)")
    }
}

extension AddViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == costEdit{
            textField.resignFirstResponder()
        }else if textField == descriptionField{
            textField.resignFirstResponder()
        }else if textField == titleField{
            textField.resignFirstResponder()
        }
        return true
    }
    
    //Textfield delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        if textField == costEdit{
            switch string {
            case "0","1","2","3","4","5","6","7","8","9":
                currentString += string
                print(currentString)
                formatCurrency(string: currentString)
            default:
                let array = Array(string)
                var currentStringArray = Array(currentString)
                if array.count == 0 && currentStringArray.count != 0 {
                    currentStringArray.removeLast()
                    currentString = ""
                    for character in currentStringArray {
                        currentString += String(character)
                    }
                    formatCurrency(string: currentString)
                }
            }
            return false
        }
        return true
    }
    
    func formatCurrency(string: String) {
        print("format \(string)")
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.locale = Locale(identifier: "en_US")
        let num = Double(currentString)
        if let currentNumber = num{
            let numberFromField = currentNumber / 100
            let nsNum = NSNumber.init(value: numberFromField)
            costEdit.text = formatter.string(from: nsNum)
            let text = costEdit.text!
            let next: String = String(text.dropFirst())
            costEdit.text = next
            print(costEdit.text!)
        }
        
    }
}
extension AddViewController : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}



