//
//  ItemSummeryViewController.swift
//  Life Items
//
//  Created by Casey Scott on 3/6/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//

import UIKit
import CoreData
import DatePickerDialog

class ItemSummeryViewController: UIViewController {
    
    let context = NSManagedObjectContext.init(concurrencyType: .mainQueueConcurrencyType)
    var catagories: [String] = []
    var selectedCategory = ""
    var delegate: UpdateListener?
    var item: RareItem?
    var currentString = ""
    var selectedLastPurchasedDate: Date = Date()
    
    
    //MARK: IBOutlets
    @IBOutlet weak var titleField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var costEdit: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var notesField: UITextView!
    @IBOutlet weak var notSavedLabel: UILabel!
    @IBOutlet weak var createdDateLabel: UILabel!
    @IBOutlet weak var lastPurchasedDateLabel: UILabel!
    
    
    //MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectedCategory = item?.category?.categoryName ?? catagories[0]
        if let createdDate = item?.createdDate{
            createdDateLabel.text = "Created on \(makeDateString(date: createdDate))"
        }
        
        //Add border to textField
        notesField.layer.borderColor = UIColor.black.cgColor
        notesField.layer.cornerRadius = 8
        notesField.layer.borderWidth = 1.5
        
        configureFields()
    }
    
    //MARK: - Actions
    
    @IBAction func closeAnythingGestureTap(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func editLastPurchasedTapped(_ sender: Any) {
        DatePickerDialog().show("Last Purchased Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                self.item?.lastPurchased = dt as NSDate
                self.lastPurchasedDateLabel.text = "Last purchased \(self.makeDateString(date: dt as NSDate))"
                self.selectedLastPurchasedDate = dt
            }
        }
    }
    
    @IBAction func editCatTapped(_ sender: Any) {
        //Present a alert with options
        let actionSheet = UIAlertController(title: "Select Category", message: "", preferredStyle: .actionSheet)
        for category in catagories{
            let option = UIAlertAction(title: category, style: .default) { (action) in
                self.item?.category?.categoryName = action.title
                self.selectedCategory = action.title ?? self.catagories[0]
                self.notSavedLabel.isHidden = false
                self.categoryLabel.text = "Category: \(self.selectedCategory)"
                actionSheet.dismiss(animated: true, completion: nil)
            }
            actionSheet.addAction(option)
        }
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func saveItemEdits(_ sender: Any) {
        if let name = titleField.text{
            if name == ""{
                self.notSavedLabel.isHidden = false
                self.titleField.shake()
                return
            }
        }
        notSavedLabel.isHidden = true
        saveEdits()
    }
    
    //MARK: - Custom Functions
    
    func saveEdits(){
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDel.persistentContainer.viewContext
        managedContext.delete(item!)
        do{
            try managedContext.save()
        }catch{
            NSLog("Unresolved error \(String(describing: error)), \(error.localizedDescription)")
        }
        let entity = NSEntityDescription.entity(forEntityName: "RareItem", in: managedContext)
        let itemMain = NSManagedObject(entity: entity!, insertInto: managedContext) as? RareItem
        
        if let item = itemMain{
            if let name = titleField.text{
                item.itemName = name
            }
            if let desc = descriptionField.text{
                item.itemDesc = desc
            }
            let date = NSDate(timeIntervalSinceNow: 0)
            item.createdDate = date
            
            if let cost = costEdit.text{
                item.cost = NSDecimalNumber(string: cost)
                print(String(describing: item.cost))
            }

            
            item.category = Category(context: managedContext)
            item.category?.categoryName = selectedCategory
            print(item.category?.categoryName as Any)
            
            item.notes = notesField.text
            
            item.lastPurchased = selectedLastPurchasedDate as NSDate
            
            do{
                try managedContext.save()
            }catch{
                print("Unresolved error \(String(describing: error)), \(error.localizedDescription)")
                
            }
        }
        delegate?.updateTable()
    }
    
    func makeDateString(date: NSDate) -> String{
        let formatter = DateFormatter()
        formatter.setLocalizedDateFormatFromTemplate("MMM dd, YYYY")
        formatter.locale = Locale(identifier: "en_US")
        return formatter.string(from: date as Date)
    }
    
    func configureFields(){
        if let name = item?.itemName{
            titleField.text = name
        }else{
            titleField.text = "Add title"
        }
        if let desc = item?.itemDesc{
            descriptionField.text = desc
        }else{
            descriptionField.text = "Add description"
        }
        if let cost = item?.cost{
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.minimumFractionDigits = 2
            formatter.maximumFractionDigits = 2
            costEdit.text = formatter.string(from: cost)
        }else{
            costEdit.text = "0.00"
        }
        if let cat = item?.category?.categoryName{
            categoryLabel.text = "Category: \(cat)"
        }
        if let note = item?.notes{
            notesField.text = note
        }
        if let cDate = item?.createdDate{
            createdDateLabel.text = "Created on \(makeDateString(date: cDate))"
        }else{
            createdDateLabel.text = "Created on N/A)"
        }
        if let lDate = item?.lastPurchased{
            lastPurchasedDateLabel.text = "Last purchased \(makeDateString(date: lDate))"
        }else {
            lastPurchasedDateLabel.text = "Last purchased - N/A"
        }
        
    }
    
}

//MARK: Class Extensions

extension ItemSummeryViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == costEdit{
          textField.resignFirstResponder()
        }else if textField == descriptionField{
            textField.resignFirstResponder()
        }else if textField == titleField{
            textField.resignFirstResponder()
        }
        return true
    }
    
    //Textfield delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool { // return NO to not change text
        if textField == costEdit{
            switch string {
            case "0","1","2","3","4","5","6","7","8","9":
                currentString += string
                print(currentString)
                formatCurrency(string: currentString)
            default:
                let array = Array(string)
                var currentStringArray = Array(currentString)
                if array.count == 0 && currentStringArray.count != 0 {
                    currentStringArray.removeLast()
                    currentString = ""
                    for character in currentStringArray {
                        currentString += String(character)
                    }
                    formatCurrency(string: currentString)
                }
            }
            return false
        }
        if textField == titleField{
            if let text = textField.text{
                if let itemName = item?.itemName{
                    if text != itemName{
                        notSavedLabel.isHidden = false
                    }else{
                        notSavedLabel.isHidden = true
                    }
                }
            }
        }
        if textField == descriptionField{
            if let text = textField.text{
                if let itemName = item?.itemDesc{
                    if text != itemName{
                        notSavedLabel.isHidden = false
                    }else{
                        notSavedLabel.isHidden = true
                    }
                }
            }
        }
        return true
    }
    
    func formatCurrency(string: String) {
        print("format \(string)")
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.currency
        formatter.locale = Locale(identifier: "en_US")
        let num = Double(currentString)
        if let currentNumber = num{
            let numberFromField = currentNumber / 100
            let nsNum = NSNumber.init(value: numberFromField)
            costEdit.text = formatter.string(from: nsNum)
            let text = costEdit.text!
            let next: String = String(text.dropFirst())
            costEdit.text = next
            print(costEdit.text!)
            
            if let cost = item?.cost{
                if costEdit.text != String(describing: cost){
                    notSavedLabel.isHidden = false
                }
            }
        }
        
    }
}

extension ItemSummeryViewController: UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text{
            if let notes = item?.notes{
                if notes != text{
                    notSavedLabel.isHidden = false
                }
                else{
                    notSavedLabel.isHidden = true
                }
            }
            
        }
    }
}

extension ItemSummeryViewController : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}



