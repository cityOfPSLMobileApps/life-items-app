//
//  DataController.swift
//  Life Items
//
//  Created by Casey Scott on 3/4/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//

//import UIKit
//import CoreData
//
//class DataController: NSObject {
//    var managedObjectContext: NSManagedObjectContext
//    init(completionClosure: @escaping () -> ()) {
//        persistentContainer = NSPersistentContainer(name: "Life_Items")
//        persistentContainer.loadPersistentStores() { (description, error) in
//            if let error = error {
//                fatalError("Failed to load Core Data stack: \(error)")
//            }
//            completionClosure()
//        }
//    }
//}

