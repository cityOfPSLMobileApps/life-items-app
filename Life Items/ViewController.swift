//
//  ViewController.swift
//  Life Items
//
//  Created by Casey Scott on 3/4/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    var items: [RareItem] = []
    var catagories = ["Home", "Office", "Automotive", "Other"]
    var allItemsDict: [String: [RareItem]] = [:]
    var homeItems: [RareItem] = []
    var officeItems: [RareItem] = []
    var autoItems: [RareItem] = []
    var otherItems: [RareItem] = []
    var selecteditem: RareItem = RareItem()
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let button1 = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(goToAdd))
        self.navigationItem.rightBarButtonItem  = button1
        
        items = getData()
        
        sortItems(it: items)
    }
    
    @objc func goToAdd(){
        performSegue(withIdentifier: "addSegue", sender: nil)
    }
    
    func getData() -> [RareItem]{
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else {return []}
        let managedContext = appDel.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "RareItem")
        do{
            let itemsArray = try managedContext.fetch(fetchRequest)
            return itemsArray as! [RareItem]
        }catch let error as NSError{
            print(error)
        }
        return []
    }

    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addSegue"{
            let destination = segue.destination as? AddViewController
            destination?.catagories = catagories
            destination?.delegate = self
        }
        else if segue.identifier == "itemSegue"{
            let destination = segue.destination as? ItemSummeryViewController
            destination?.catagories = catagories
            destination?.delegate = self
            destination?.item = selecteditem
        }
      
    }
    
    @IBAction func unwindToMain(segue: UIStoryboardSegue){
        
    }
    
    func sortItems(it: [RareItem]){
        
        for i in it{
            switch i.category?.categoryName{
            case catagories[0]:
                print(catagories[0])
                homeItems.append(i)
                allItemsDict.updateValue(homeItems, forKey: catagories[0])
                homeItems.sort { (a, b) -> Bool in
                    a.itemName! < a.itemName!
                }
            case catagories[1]:
                print(catagories[1])
                officeItems.append(i)
                allItemsDict.updateValue(officeItems, forKey: catagories[1])
            case catagories[2]:
                print(catagories[2])
                autoItems.append(i)
                allItemsDict.updateValue(autoItems, forKey: catagories[2])
            case catagories[3]:
                print(catagories[3])
                otherItems.append(i)
                allItemsDict.updateValue(otherItems, forKey: catagories[3])
            default:
                print("default")
            }
        }
    }
    
    func sortedArray(_ ar: [RareItem]) -> [RareItem]{
       return ar.sorted { (a, b) -> Bool in
            a.itemName! < b.itemName!
        }
    }

}

extension ViewController : UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if items.count == 0{
            return 0
        }
        return catagories.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return catagories[section]
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if items.count == 0{
            return 0
        }
        if allItemsDict[catagories[section]] == nil{
            return 0
        }
        return allItemsDict[catagories[section]]!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ItemsTableViewCell
        cell.titleLabel.text = allItemsDict[catagories[indexPath.section]]![indexPath.row].itemName
        cell.descLabel.text = allItemsDict[catagories[indexPath.section]]![indexPath.row].itemDesc
        if cell.descLabel.text! == noDescription{
            cell.descLabel.textColor = UIColor.lightGray
        }else if cell.descLabel.text! == ""{
            cell.descLabel.textColor = UIColor.lightGray
            cell.descLabel.text = noDescription
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        
        return .delete
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .default, title: "Delete") { (delete, indexpath) in
            guard let appDel = UIApplication.shared.delegate as? AppDelegate else {return}
            let managedContext = appDel.persistentContainer.viewContext
            managedContext.delete(self.allItemsDict[self.catagories[indexPath.section]]![indexPath.row])
            do{
                try managedContext.save()
                self.updateTable()
            }catch{
                NSLog("Unresolved error \(String(describing: error)), \(error.localizedDescription)")
            }
        }
        
        return [delete]
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            print("Item from \(catagories[0]) selected")
            selecteditem = homeItems[indexPath.row]
            print(selecteditem)
        case 1:
            print("Item from \(catagories[1]) selected")
            selecteditem = officeItems[indexPath.row]
        case 2:
            print("Item from \(catagories[2]) selected")
            selecteditem = autoItems[indexPath.row]
        case 3:
            print("Item from \(catagories[3]) selected")
            selecteditem = otherItems[indexPath.row]
        default:
            print("default item selected = Nothing")
        }
        print(selecteditem)
        performSegue(withIdentifier: "itemSegue", sender: nil)
    }
    
}

extension ViewController: UpdateListener{
    func updateTable() {
        items = []
        homeItems = []
        autoItems = []
        officeItems = []
        otherItems = []
        allItemsDict.removeAll()
        items = getData()
        sortItems(it: items)
        tableView.reloadData()
    }
}

