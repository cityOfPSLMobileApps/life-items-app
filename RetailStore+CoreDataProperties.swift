//
//  RetailStore+CoreDataProperties.swift
//  Life Items
//
//  Created by Casey Scott on 3/4/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//
//

import Foundation
import CoreData


extension RetailStore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RetailStore> {
        return NSFetchRequest<RetailStore>(entityName: "RetailStore")
    }

    @NSManaged public var storeName: String?
    @NSManaged public var storeDesc: String?
    @NSManaged public var items: NSSet?

}

// MARK: Generated accessors for items
extension RetailStore {

    @objc(addItemsObject:)
    @NSManaged public func addToItems(_ value: RareItem)

    @objc(removeItemsObject:)
    @NSManaged public func removeFromItems(_ value: RareItem)

    @objc(addItems:)
    @NSManaged public func addToItems(_ values: NSSet)

    @objc(removeItems:)
    @NSManaged public func removeFromItems(_ values: NSSet)

}
