//
//  RareItem+CoreDataProperties.swift
//  Life Items
//
//  Created by Casey Scott on 3/4/19.
//  Copyright © 2019 Casey Scott. All rights reserved.
//
//

import Foundation
import CoreData


extension RareItem {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RareItem> {
        return NSFetchRequest<RareItem>(entityName: "RareItem")
    }

    @NSManaged public var cost: NSDecimalNumber?
    @NSManaged public var itemName: String?
    @NSManaged public var itemDesc: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var notes: String?
    @NSManaged public var lastPurchased: NSDate?
    @NSManaged public var category: Category?
    @NSManaged public var stores: NSSet?

}

// MARK: Generated accessors for stores
extension RareItem {

    @objc(addStoresObject:)
    @NSManaged public func addToStores(_ value: RetailStore)

    @objc(removeStoresObject:)
    @NSManaged public func removeFromStores(_ value: RetailStore)

    @objc(addStores:)
    @NSManaged public func addToStores(_ values: NSSet)

    @objc(removeStores:)
    @NSManaged public func removeFromStores(_ values: NSSet)

}
